<?php
/**
 * elFinder is a powerful AJAX file manager
 * Copyright (c) 2010-2015, Alexey Sukhotin
 */

use Drupal\file\Plugin\Core\Entity\File;
use Drupal\Core\Block\BlockPluginInterface;
include_once dirname(elfinder_connector_path()) . DIRECTORY_SEPARATOR . 'elFinder.class.php';
use Drupal\elfinder\Controller\elFinderPermissions;
use Drupal\elfinder\Controller\elFinderPageController as elFinderPageController;


function elfinder_check_badpaths() {
  $ret = array('result' => TRUE, 'message' => '');
  $libpath = elfinder_lib_path();
  $badfiles = array('elfinder.html', 'elfinder.php.html', 'connectors/php/connector.php', 'php/connector.php', 'php/connector.minimal.php');

  $badpaths = array();

  foreach ($badfiles as $badfile) {
    $path = $libpath . '/' . $badfile;
    if (file_exists($path)) {
      $badpaths[] = $path;
    }
  }

  if (count($badpaths) > 0) {
    $message = t('elFinder library directory containing features test files :files. Please remove these files to avoid security hole.', array(':files' => implode(', ', $badpaths)));
    //watchdog_exception('elfinder', $e, $message, array());
    drupal_set_message($message, 'error');
    
    $ret['result'] = FALSE;
    $ret['message'] = $message;
    
  }

  return $ret;
}

function elfinder_init() {
  elfinder_check_badpaths();
}


function elfinder_get_disabled_commands() {

  $disabled_commands = array();

  $permissions = elFinderPermissions::permissions();
  
  $commands = elfinder_cmd_permissions();

  // Disable not allowed commands
  foreach ($permissions as $perm_name => $perm_data) {
    if (array_key_exists($perm_name, $commands) && !\Drupal::currentUser()->hasPermission($perm_name)) {
      array_push($disabled_commands, $commands[$perm_name]);
    }
  } 
   
/*  if (!user_access('download own uploaded files') && !user_access('download all uploaded files')) {
    $disabled_commands[] = 'getfile';
  }*/
 
  return $disabled_commands;
}

function elfinder_user_roles() {
  $user =  \Drupal::currentUser();
  return $user->getRoles();
}

function elfinder_get_user_profile() {

  $profiles = entity_load_multiple('elfinder_profile');

  $first_profile = NULL;
  
  $rids = elfinder_user_roles();
 // $rids[] = 1; // anonymous user


  
  /* Get first matching profile for higest weight user role */
  foreach ($rids as $rid) {
    foreach ($profiles as $profile) {


        if (in_array($rid, $profile->getConf('roles')) or in_array('anonymous', $profile->getConf('roles'))) {
        
          if (is_null($first_profile)) {
            $first_profile = $profile;
          } else if (isset($profile->settings['volume'])) {
            /* Adding all available to current user volumes from other profiles */
           /* foreach ($profile->settings['volume'] as $volume) {
              $first_profile->settings['volume'][] = $volume;
            }*/
          }
        }
    }
    
  }

  return $first_profile;
}



function elfinder_elfinder_bind() {
  return array('test' => 'test2');
}


/**
 * elFider commandsto permissions assignment
 */
function elfinder_cmd_permissions() {
  $perm = array(
    'create new directories'        => 'mkdir',
    'create new files'              => 'mkfile',
    'rename files and directories'  => 'rename',
    'file uploads'                  => 'upload',
    'copy to clipboard'             => 'copy',
    'cut to clipboard'              => 'cut',
    'paste from clipboard'          => 'paste',
    'preview files'                 => 'quicklook',
    'view file info'                => 'info',
    'delete files and directories'  => 'rm',
    'duplicate files'               => 'duplicate',
    'edit files'                    => 'edit',
    'add files to archive'          => 'archive',
    'extract files from archive'    => 'extract',
    'resize images'                 => 'resize',
    'download own uploaded files'   => 'download',
    'view file description'         => 'desc',
    'edit file description'         => 'editdesc',
    'view file owner'               => 'owner',
    'view file downloads'           => 'downloadcount',
    'mount network volumes'         => 'netmount',
  );
  
  $newcperms =  \Drupal::moduleHandler()->invokeAll('elfinder_cmd_perms', $perm);
  
  return ($newcperms) ? $newcperms : $perm;
}

/**
 * Implements hook_wysiwyg_plugin().
 */
function elfinder_wysiwyg_plugin($editor, $version) {

  $path = drupal_get_path('module', 'elfinder');

  drupal_add_js($path . '/js/elfinder.wysiwyg.init.js', array('group' => JS_LIBRARY));
  
  $options = array(
    'elfinder_url' => \Drupal::url('elfinder', array('query' => array('app' => $editor ))),
    'homepage_url' => elfinder_homepage_url(),
    'path'         => $path,
  );
  
  /* bueditor hack */
  if (preg_match('/^(bueditor|bue)/', $editor)) {
      $editor = 'bueditor';
  }
  
  // FIXME: $editor_plugins = elfinder_system_listing("$editor\.inc\$", $path . '/editors' , 'name', 0);
  
  $editor_plugins =  array();
  
  /* loading editor plugin */
  $plugin_file = '';
  
  if (isset($editor_plugins[$editor])) {
  
    $plugin_file = $editor_plugins[$editor]->uri;
 
    if ($plugin_file) {
      $options['plugin_url_base'] = "$path/editors/$editor"; 
      require_once $plugin_file;
    }
  }

  /* hook for adding another editor support */
  //\Drupal::moduleHandler()->invokeAll('elfinder_editor_plugin');
  
  /* wysiwyg plugin generation func for selected editor */
  $pluginfunc = 'elfinder_' . $editor . '_elfinder_editor_plugin';
  
  /* returning elFinder wysiwyg editor plugin for selected editor */
  if (function_exists($pluginfunc)) {
    return call_user_func_array($pluginfunc, array($options));
  }

}

/**
 * library homepage url
 */
function elfinder_homepage_url() {
  return 'http://elfinder.org/';
}

/**
 * library download url
 */
function elfinder_download_url() {
  return 'http://sourceforge.net/projects/drupal-elfinder/files/Library/';
}

/**
 * library files path
 */
function elfinder_lib_path($name = 'elfinder') {

  $path = drupal_get_path('module', 'elfinder') . '/vendor/' . $name;

  return $path;
}


/**
 * library to filesystem connector path
 */
function elfinder_connector_path() {
  return elfinder_lib_path() . '/php/elFinder.class.php';
}

/**
  * path to files directory
  */
function elfinder_file_directory_path($absolute = FALSE) {
  $path = \Drupal::config('elfinder.settings')->get('filesystem.root_custom');
  $path = elfinder_parse_path_tokens($path);

  return $path;
}

/**
  * Returning site document root
  */
function elfinder_document_root() {
  return realpath('');
}


/**
  * replace path tokens to its values (%uid, %user, etc)
  */
function elfinder_parse_path_tokens($unparsed_path) {
  global $language;
  $path = $unparsed_path;
  $user = \Drupal::currentUser();
  $uname = isset($user->name) ? preg_replace('/[[:^alnum:]]+/', '', $user->name) : '';
  $langCode = isset($language->language) ? $language->language : 'en';
  
  $path_tokens = array(
    '%uid' => $user->id(),
    '%name' => $uname,
    '%user' => $uname,
    '%files' => elfinder_default_directory_path(),
    '%lang' => $langCode,
  );

  $path = strtr($path, $path_tokens);

  if (function_exists('token_replace')) {
      $node = node_load($user->id());
      $path = token_replace($path, array('node' => $node, 'user' => $user));
  }
  
  return $path;
}

/**
  * default path to files directory
  */
function elfinder_default_directory_path() {
  $filepath = '';

  $scheme = file_default_scheme();
    
  if ($scheme == 'public') {
    $filepath = \Drupal::config('elfinder.settings')->get('file_public_path');  
  }
  else {
    $filepath = \Drupal::config('elfinder.settings')->get('file_private_path');
  }
  
  return $filepath;
}

/**
  * files directory url
  */
function elfinder_file_directory_url($force_relative = FALSE) {
  global $base_root;

  $fileurl = '';

  $fileurl = \Drupal::config('elfinder.settings')->get('filesystem.url_custom') . '/';
  if ($fileurl) {
    $fileurl = elfinder_parse_path_tokens($fileurl);
  }

  // Cutting off absolute url prefix
  if ($force_relative) {
    $fileurl = str_replace($base_root, '', $fileurl);
  }
  
  return $fileurl;
}

/**
  * prepare directory for files
  */
function elfinder_prepare_directory($directory) {
  $rc = file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
  return $rc;
}


function elfinder_system_listing($matchexpr, $path, $arg1, $arg2) {
    //return drupal_system_listing("/$matchexpr/", $path, $arg1, $arg2);
}

function elfinder_get_drupal_file_obj($uri) {
    $user =  \Drupal::currentUser();

    $file = NULL;
    
    $fid = db_select('file_managed', 'f')
      ->condition('uri', rawurldecode($uri))
      ->fields('f', array('fid'))
      ->execute()
      ->fetchField();

    if ($fid) {
      $file = file_load($fid);
    } 
    else {
     $file_init = array(
        'uid' => $user->id(),
        'filename' => basename($uri),
        'uri' => $uri,
        'filemime' => \Drupal::service('file.mime_type.guesser')->guess($uri),
        'timestamp' => REQUEST_TIME,
        'status' => FILE_STATUS_PERMANENT,
      );

      if (is_file(drupal_realpath($uri))) {
        $file_init['filesize'] = filesize($uri);
       
        list($type) = explode("/", $file_init['filemime']);

        switch($type) {
          case 'image':
          case 'video':
            $file_init['type'] = $type;
            break;

          default:
            $file_init['type'] = 'default';
            break;
        }
      }

      $file = entity_create('file', $file_init);
    }
    return $file;
}

function elfinder_file_download($uri) {
  if (config('elfinder.settings')->get('filesystem.handleprivate') == 'true') {
    $user =  \Drupal::currentUser();
    
    $accessown = 'download own uploaded files';
    $accessall = 'download all uploaded files';
    
    $info = elfinder_get_drupal_file_obj($uri);
    if (($info->uid == $user->id() && user_access($accessown)) || user_access($accessall)) {
    
     $disposition = preg_match("/^(text|image)\//", $info->filemime) ? 'inline' : 'attachment';

      return array(
        'Content-Type' => $info->filemime,
        'Content-Length' => $info->filesize,
        'Content-Disposition' => elFinderDrupal::GetContentDisposition($info->filename, $info->filemime, FALSE),
      );
    
    }
 
    return NULL;
  }
}

function template_preprocess_elfinder_page(&$variables) {
  global $language;
  $variables['elfinder_lang'] = isset($language->language) ? $language->language : 'en';
  $variables['elfinder_title'] = t('File Browser');
  $variables['elfinder_messages'] = 'TBD'; //theme('status_messages');
}

function elfinder_page_rrender($variables) {
  return \Drupal::service('renderer')->render($variables['page']);
}


function elfinder_theme() {
    /* !!! Do NOT name it 'elfinder_page'  !!! */
  return array(
    'browser_page' => array(
      'variables' => array(
        'alpha1' => array(),
      ),
   // 'path' => drupal_get_path('module') . '/templates',
    'template' => 'browser-page'

      //'function' => 'elfinder_page_render',
    ),
  );
}

/* For global js insertion */
function elfinder_page_attachments_alter(array &$page) {
  $browserpage = elFinderPageController::buildBrowserPage(TRUE);
  $page['#attached']['library'][] = 'elfinder/drupal.elfinder';
  $page['#attached']['drupalSettings']['elfinder'] = $browserpage['#attached']['drupalSettings']['elfinder'];
}